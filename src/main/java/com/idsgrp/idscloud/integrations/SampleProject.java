package com.idsgrp.idscloud.integrations;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class SampleProject {

    public static void main(String[] args) {
        try {
            String accessToken  = getAccessToken();
            System.out.println(accessToken);

            String rateTableResponse = exportRateTable(accessToken, "term");
            System.out.println(rateTableResponse);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static String getAccessToken() throws IOException, InterruptedException, ExecutionException {
        // Client Creation
        HttpClient client = HttpClient.newHttpClient();

        String tokenUrl = "https://api.customer1dev.idscloud.io/token-auth/token";

        // Subsitute in your client_id, client_secret, and audience in place of < > placeholders.
        String tokenRequestBody = "{\"client_id\":\"<your client_id here>\", \"client_secret\":\"<your client_secret here>\", \"audience\":\"<your audience here>\", \"grant_type\":\"client_credentials\"}";

        // Request creation
        HttpRequest request = HttpRequest.newBuilder(
                URI.create(tokenUrl))
                .header("content-type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(tokenRequestBody))
                .build();

        // send the request
        HttpResponse<String> response = client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).get();


        if (response.statusCode() != 200) {
            throw new RuntimeException("Failure in call to generate access token");
        }

        // conversion to a map for easier json interactions
        HashMap result = new ObjectMapper().readValue(response.body(), HashMap.class);
        return (String) result.get("access_token");
    }


    private static String exportRateTable(String accessToken, String rateTableDesc) throws IOException, InterruptedException, ExecutionException {
        // Client Creation
        HttpClient client = HttpClient.newHttpClient();

        // Subsitute in your url here. Example url is in format: "https://api.<customer environment>.idscloud.io/pricing/api/IAQEExportRateTable/exportratetable"
        String exportRateTableUrl = "<your export rate table url here>";
        String rateTableRequestBody = "{ \"rate_table_export_request\": { \"rate_tables\": { \"rate_table_desc\": \"" + rateTableDesc + "\" } }}";

        // Request creation
        HttpRequest request = HttpRequest.newBuilder(
                URI.create(exportRateTableUrl))
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + accessToken)
                .POST(HttpRequest.BodyPublishers.ofString(rateTableRequestBody))
                .build();

        // send the request
        HttpResponse<String> response = client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).get();

        return response.body();
    }

}
