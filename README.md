# sample-java-api-implementation

This project is a sample java implementation of an IDScloud API. It contains an example of how to generic an access token, and use that token in a call to the "exportratetable" resource in the IDScloud Pricing Microservice API.

## Prerequisites
* Java 11+
* Client_id, client_secret, audience for the environment you are connecting to.
* API URL for the environment you are connecting to.